from PyQt5.QtWidgets import QApplication

import sys

from src import XmlMainWindow, XmlReader, XmlDockingWindow, XmlTreeView, XmlHandler

class XmlStand():

    def __init__(self, data = None):
        self.window = XmlMainWindow()
        self.initTreeView()
        self.initReader()
        self.signalsAndSlots()

        self.reader.read('./data/example.xml')
        return

    def initReader(self):
        self.reader = XmlReader()
        return
    
    def initTreeView(self):
        self.treeView = XmlTreeView()
        self.tqreeDock = XmlDockingWindow(self.treeView, "xml Ansicht", self.window)
        return

    def signalsAndSlots(self):
        self.window.openFileRecieved.connect(self.reader.read)
        self.window.saveFileRecieved.connect(self.reader.write)
        self.window.newXmlSignal.connect(self.new)
        self.window.collapseSignal.connect(self.treeView.contextCollapse)
        self.window.expandSignal.connect(lambda: self.treeView.contextCollapse(item=None, collapse=True))
        self.reader.newXmlInput.connect(self.treeView.populateTree)
        return

    def new(self):
        self.reader.read('./data/empty.xml')

    def show(self):
        self.window.show()
        return
        

def main():
    app = QApplication(sys.argv)

    xmlStand = XmlStand()
    xmlStand.show()
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()