from PyQt5 import QtCore, QtGui, QtXml, QtWidgets

from .XmlElement import XmlElement
from .XmlData import XmlData
from .XmlReader import XmlReader

import os

class XmlTreeView(QtWidgets.QTreeWidget):

    def __init__(self):
        QtWidgets.QTreeWidget.__init__(self)
        self.xml = None
        self.initMainWindow()
        self.initContextMenu()

        self.itemChanged.connect(self.editEntry)
        self.currentItemChanged.connect(self.getEntry)

        self.setExpandsOnDoubleClick(True)
        self.header().setSectionResizeMode(0)
        self.setColumnWidth(0, super().frameGeometry().width()*0.4)
        
        self.createShortcuts()
        return

    def createShortcuts(self):
        QtWidgets.QShortcut(
            QtGui.QKeySequence.MoveToPreviousLine, self
        ).activated.connect(
            lambda: self.setCurrentItem(
                self.itemAbove(self.currentItem())
            ) if self.itemAbove(self.currentItem()) else None
        )
        QtWidgets.QShortcut(
            QtGui.QKeySequence.MoveToNextLine, self
        ).activated.connect(
            lambda: self.setCurrentItem(
                self.itemBelow(self.currentItem())
            ) if self.itemBelow(self.currentItem()) else None
        )
        QtWidgets.QShortcut(
            QtGui.QKeySequence.Delete, self
        ).activated.connect(
            self.contextRemoveElement
        )
        QtWidgets.QShortcut(
            'Ctrl+Shift+I', self
        ).activated.connect(
            self.contextInsertDevice
        )
        QtWidgets.QShortcut(
            'Ctrl+I', self
        ).activated.connect(
            self.contextInsertAttribute
        )
        QtWidgets.QShortcut(
            'F5', self
        ).activated.connect(
            lambda: 
                self.populateTree(self.xml) if self.xml is not None else None
        )
        QtWidgets.QShortcut(
            'Ctrl+E', self
        ).activated.connect(
            lambda: 
            [
                self.editItem(self.currentItem(), 1)
            ]
        )
        return

    def initMainWindow(self):
        self.header().setSectionResizeMode(QtWidgets.QHeaderView.Stretch)
        self.setHeaderLabels(['Name', 'Wert'])
        return

    def initContextMenu(self):
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.openMenu)
        return

    def openMenu(self, position):
        self.activeElement = self.currentItem()
        self.currentData = [self.currentItem().data(0,0), self.currentItem().data(1,0)]
       
        menu = QtWidgets.QMenu()
        if isinstance(self.activeElement, XmlTreeItem):
            self.addContextAction(menu, 'Device einfügen', self.contextInsertDevice, 'Ctrl+Shift+I')
            self.addContextAction(menu, 'Attribut einfügen', self.contextInsertAttribute, 'Ctrl+I')
            menu.addSeparator()
            self.addContextAction(menu, 'Kopieren', self.copyEvent, 'Ctrl+C')
            self.addContextAction(menu, 'Einfügen', self.pasteEvent, 'Ctrl+V')
            if self.activeElement.parent():
                self.addContextAction(menu, 'Entfernen', self.contextRemoveElement, 'Entf')
            menu.addSeparator()
            self.addContextAction(menu, 'Einklappen', self.contextCollapse, 'Left')
            self.addContextAction(menu, 'Ausklappen', lambda x: self.contextCollapse(None, True), 'Right')
            menu.addSeparator()
            self.addContextAction(menu, 'Device importieren ...', self.contextImportDevice)
            self.addContextAction(menu, 'Device exportieren ...', self.contextExportDevice)
        else:
            self.addContextAction(menu, 'Kopieren', self.copyEvent, 'Ctrl+C')
            self.addContextAction(menu, 'Einfügen', self.pasteEvent, 'Ctrl+V')
            self.addContextAction(menu, 'Entfernen', self.contextRemoveElement, 'Entf')
        menu.exec_(self.viewport().mapToGlobal(position))

    def addContextAction(self, menu, name, function, shortcut=None, icon=None):
        action = QtWidgets.QAction(self.tr(name), self)
        action.triggered.connect(function)
        if shortcut:
            action.setShortcut(shortcut)
        if icon:
            action.setIcon(icon)
        menu.addAction(action)
        return action
    
    def contextInsertDevice(self):
        root = self.currentItem()
        if not isinstance(root, XmlTreeItem):
            root = root.parent()
        rootElement = root.getElement()
        newDevice = XmlElement('NewDevice')
        self.xml.add(newDevice)
        rootElement.addChild(newDevice)
        newItem = self.insertElement(newDevice, root)
        self.setCurrentItem(newItem)
        self.editItem(newItem)
        return

    def contextInsertAttribute(self):
        self.blockSignals(True)

        root = self.currentItem()
        if not isinstance(root, XmlTreeItem):
            root = root.parent()
        rootElement = root.getElement()

        name = 'NewAttribute'
        count = 1
        while name in rootElement.attributes.keys():
            name = 'NewAttribute_'+str(count)
            count += 1
        rootElement.set(name, 'Wert')

        attributeItem = QtWidgets.QTreeWidgetItem(root)
        attributeItem.setText(0, name)
        attributeItem.setText(1, rootElement.get(name))
        
        self.blockSignals(False)
        attributeItem.setFlags(attributeItem.flags() | QtCore.Qt.ItemIsEditable)
        self.setCurrentItem(attributeItem)
        self.editItem(attributeItem)
        return

    def contextRemoveElement(self):
        self.blockSignals(True)
        root = self.currentItem()
        if root.parent() is None:
            return
        if isinstance(root, XmlTreeItem):
            self.xml.remove(root.getElement())
        else:
            root.parent().getElement().attributes.pop(self.currentItem().data(0,0))
        root = self.invisibleRootItem()
        for item in self.selectedItems():
            (item.parent() or root).removeChild(item)
        self.blockSignals(False)
        return
        
    def contextCollapse(self, item=None, collapse=False):
        if not item:
            item = self.currentItem()

        for child in [item.child(i) for i in range(item.childCount())]:
            self.contextCollapse(child, collapse)

        item.setExpanded(collapse)
        return

    def contextImportDevice(self):
        self.blockSignals(True)
        item = self.currentItem()
        
        path = QtWidgets.QFileDialog.getOpenFileName(self,"Device speichern", os.path.normpath("./devices/"),"xml Datei (*.xml)")[0]
        
        if path:
            loadReader = XmlReader()
            try:
                xml2import = loadReader.read(path)
            except:
                return

            element2import = xml2import.getContent()[0]
            self.xml.add(element2import)
            newItem = self.insertElement(element2import, item)
            self.setCurrentItem(newItem)
        self.blockSignals(False)
        return

    def contextExportDevice(self):
        item = self.currentItem()
        name = 'device'
        if item.getElement().get('category'):
            name = item.getElement().get('category')
        path = QtWidgets.QFileDialog.getSaveFileName(self,"Device speichern", os.path.normpath("./devices/{}.xml".format(name)),"xml Datei (*.xml)")[0]
        
        if path:
            element2save = item.getElement().getCopy()

            data2save = XmlData()
            data2save.add(element2save)

            saveReader = XmlReader()
            saveReader.write(path, data2save)
        return

    def populateTree(self, xml=None):
        self.blockSignals(True)
        if xml:
            self.xml = xml
        data = self.xml.getContent()
        self.clear()
        self.insertElement(data[0], self)
        self.setCurrentItem(self.topLevelItem(0))
        self.blockSignals(False)
        return

    def insertElement(self, element, parent):
        newItem = XmlTreeItem(element, parent)
        for child in element.getChild():
            self.insertElement(child, newItem)
        return newItem

    def getEntry(self):
        if self.currentItem():
            self.currentData = self.currentItem().data(0,0)
        return

    def editEntry(self, item):
        if item:
            if not isinstance(item, XmlTreeItem):
                attribute = item.data(0,0)
                value = item.data(1,0)
                item = item.parent()
                if attribute not in item.getElement().attributes.keys():
                    item.getElement().attributes.pop(self.currentData)
            else: 
                attribute = 'category'
                value = item.data(0,0)
            
            item.getElement().set(attribute, value)
        self.getEntry()
        return 

    def keyPressEvent(self, qkeyevent):
        
        if qkeyevent.matches(QtGui.QKeySequence.Copy):
            self.copyEvent()
        if qkeyevent.matches(QtGui.QKeySequence.Paste):
            self.pasteEvent()
        if qkeyevent.matches(QtGui.QKeySequence.Cut):
            self.cutEvent()

    def copyEvent(self):
        item = self.currentItem()
        if item.parent() is None:
                self.buffer = None
                return

        if isinstance(item, XmlTreeItem):
            self.buffer = ['device', item.getElement().getCopy()]
        else:
            self.buffer = ['attribute', item]

    def pasteEvent(self):
        item = self.currentItem()
        if not self.buffer:
            return

        self.blockSignals(True)
        if not isinstance(item, XmlTreeItem):
            item = item.parent()
            
        rootElement = item.getElement()
        
        if self.buffer[0] == 'device':
            newDevice = self.buffer[1]
            rootElement.addChild(newDevice)
            self.xml.add(newDevice)
            self.insertElement(newDevice, item)
        
        if self.buffer[0] == 'attribute':
            attribute = self.buffer[1].data(0,0)+'_copy'
            value = self.buffer[1].data(1,0)

            counter = 1
            while attribute in item.getElement().attributes.keys():
                attribute = self.buffer[1].data(0,0)+'_copy{}'.format(counter)
                counter += 1

            item.getElement().set(attribute, value)
            attributeItem = QtWidgets.QTreeWidgetItem(item)
            attributeItem.setText(0, attribute)
            attributeItem.setText(1, rootElement.get(attribute))
            attributeItem.setFlags(attributeItem.flags() | QtCore.Qt.ItemIsEditable)
        self.blockSignals(False)
        return

    def cutEvent(self):
        self.copyEvent()
        self.contextRemoveElement()
        return

class XmlTreeItem(QtWidgets.QTreeWidgetItem):

    def __init__(self, element, parent):
        self.element = element

        super(XmlTreeItem, self).__init__(parent)
        self.setText(0, element.get('category'))
        self.setExpanded(True)
        self.setFlags(self.flags() | QtCore.Qt.ItemIsEditable)

        for attribute in element.attributes:
            attributeItem = QtWidgets.QTreeWidgetItem(self)
            attributeItem.setText(0, attribute)
            attributeItem.setText(1, element.get(attribute))
            attributeItem.setFlags(attributeItem.flags() | QtCore.Qt.ItemIsEditable)
            attributeItem.setFlags(attributeItem.flags() ^ QtCore.Qt.ItemIsDropEnabled)

        self.id = element.get('id')

        return

    def getElement(self):
        return self.element