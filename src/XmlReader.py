from .XmlData import XmlData
from .XmlElement import XmlElement

from operator import methodcaller

from PyQt5.QtCore import pyqtSlot, pyqtSignal, QObject

class XmlReader(QObject):

    newXmlInput = pyqtSignal(XmlData)

    def __init__(self):
        super(XmlReader, self).__init__()
        self.xml = None
        return

    def read(self, file):
        lines = self.getLines(file)
        content = list(map(self.scanLine, lines))
        
        self.xml = XmlData()
        parents = [XmlElement('document')]
        for element in content:

            if element['content'] is not None:
                entry.set(element['start'], element['content'])

            else:
                
                if element['start'] is not None:
                    entry = XmlElement(element['start'])
                    entry.set('hierachy', len(parents))
                    self.xml.add(entry)

                    parents[-1].addChild(entry)
                    parents.append(entry)

                if element['end'] is not None:
                    del parents[-1]
        
        self.newXmlInput.emit(self.xml)
        return self.xml

    def write(self, file, xml = None):
        if not file.endswith('.xml'):
            file  = file + '.xml'
        if not xml:
            content = self.xml.getContent()
        else:
            content = xml.getContent()
        parent = content[0]

        output = open(file, 'w')
        self.writeElement(output, parent)
        return

    def writeElement(self, file, element):
        file.write('{}<{}>\n'.format('\t'*int(element.get('hierachy')-1), element.get('category')))

        for attribute in element.attributes:
            file.write('{}<{}>{}</{}>\n'.format(
                '\t'*int(element.get('hierachy')),
                attribute,
                element.get(attribute),
                attribute
            ))

        for child in element.getChild():
            self.writeElement(file, child)
        
        file.write('{}</{}>\n'.format('  '*int(element.get('hierachy')-1), element.get('category')))
        return

    def getLines(self, file):
        data = open(file, 'r')
        lines = data.readlines()
        lines = map(methodcaller("strip", "\n "), lines)
        return lines

    def scanLine(self, line):
        tags = {'start': None, 'content': None, 'end': None}
        start = 0
        end = 0

        while end < len(line)-1:
            start = line.find('<', end)
            end = line.find('>', start)
            if line[start + 1] == '/':
                tags['end'] = line[start + 2: end]
            else:
                tags['start'] = line[start + 1: end]

        if tags['start'] is not None and tags['end'] is not None:
            tags['content'] = line[line.find('>')+1 : line.rfind('<')].strip()
        
        return tags

    def getXml(self):
        return self.xml
    