from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QDockWidget


class XmlDockingWindow(QDockWidget):
    def __init__(self, widget, name="None", parent=None, position=Qt.LeftDockWidgetArea):
        
        super(XmlDockingWindow,self).__init__(name, parent)
        self.setObjectName(name)
        
        self._widget = widget
        self.setWidget(widget)

        self.setFloating(False)
        parent.addDockWidget(position, self)
    
    def getChild(self):
        return self._widget