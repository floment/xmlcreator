from PyQt5.QtCore import Qt, QSettings, pyqtSignal
from PyQt5.QtWidgets import QMainWindow, QDockWidget, QDesktopWidget, QFileDialog
import os.path

class XmlMainWindow(QMainWindow):

    openFileRecieved = pyqtSignal(str)
    saveFileRecieved = pyqtSignal(str)
    newXmlSignal = pyqtSignal()
    expandSignal = pyqtSignal()
    collapseSignal = pyqtSignal()

    def __init__(self, parent = None):
        self.mainWindowInit(parent)
        self.readSettings()
        self.initMenuBar()
        return
    
    def mainWindowInit(self, parent):
        super(XmlMainWindow, self).__init__(parent)
        self.setGeometry(QDesktopWidget().availableGeometry())
        self.setWindowTitle("xmlStand")
        self.getWindowState()
        return

    def readSettings(self):
        self.settings = QSettings(os.path.normpath('./data/settings.ini'), QSettings.IniFormat)
        if not self.settings.value("geometry") == None:
            self.restoreGeometry(self.settings.value("geometry"))
        if not self.settings.value("windowState") == None:
            self.restoreState(self.settings.value("windowState"))
        return

    def initMenuBar(self):
        bar = self.menuBar()

        fileMenu = bar.addMenu('Datei')
        fileMenu.addAction('Neuer Teststand', self.newXml, 'Ctrl+N')
        fileMenu.addSeparator()
        fileMenu.addAction('Öffnen...', self.openXml, 'Ctrl+O')
        fileMenu.addAction('Speichern...', self.saveXml, 'Ctrl+S')

        viewMenu = bar.addMenu("Ansicht")
        viewMenu.addAction('Elemente einklappen', self.collapseElements, 'Left')
        viewMenu.addAction('Elemente ausklappen', self.expandElements, 'Right')
        viewMenu.addSeparator()
        viewMenu.addAction("Fensterlayout speichern", self.saveWindowState)
        viewMenu.addAction("Fensterlayout widerherstellen", self.getWindowState)

        self.statusBar()
        return

    def newXml(self):
        self.newXmlSignal.emit()
        return

    def openXml(self):
        path = QFileDialog.getOpenFileName(self,"xmlStand öffnen", "","xml Datei (*.xml)")[0]
        if os.path.isfile(path):
            self.openFileRecieved.emit(path)
        return

    def saveXml(self):
        path = QFileDialog.getSaveFileName(self,"xmlStand speichern", "xmlStand.xml","xml Datei (*.xml)")[0]
        self.saveFileRecieved.emit(path)
        return

    def undo(self):
        return

    def redo(self):
        return

    def collapseElements(self):
        self.collapseSignal.emit()
        return

    def expandElements(self):
        self.expandSignal.emit()

    def saveWindowState(self):
        self.settings.setValue("geometry", self.saveGeometry())
        self.settings.setValue("windowState", self.saveState())
        return

    def getWindowState(self):
        self.settings = QSettings(os.path.normpath('./data/settings.ini'), QSettings.IniFormat)
        self.restoreState(self.settings.value("windowState"))
        return