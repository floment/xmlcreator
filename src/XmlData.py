from .XmlElement import XmlElement
from PyQt5.QtCore import QObject

class XmlData(QObject):

    def __init__(self):
        super(XmlData, self).__init__()
        self.content = []
        self.id = 0
        return

    def add(self, element):
        element.set('id', self.id)
        self.id += 1
        self.content.append(element)
        for child in element.getChild():
            if child not in self.content:
                self.add(child)
        return

    def getContent(self):
        return self.content
    
    def remove(self, element):
        for child in element.getChild():
            self.content.remove(child)

        element.get('parent').removeChild(element)
        self.content.remove(element)
        return

    