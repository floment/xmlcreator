from PyQt5.QtCore import QObject

class XmlElement(QObject):

    def __init__(self, category):

        super(XmlElement, self).__init__()
        self.attributes = {}
        self.meta = {'category': category, 'hierachy': None, 'parent': None, 'id': None}
        self.children = []
        return

    def set(self, key, value):
        if key in self.meta.keys():
            self.meta[key] = value
        else:
            self.attributes[key] = value
        return

    def get(self, key):
        if key in self.meta.keys():
            return self.meta[key]
        else:
            return self.attributes[key]

    def addChild(self, child):
        self.children.append(child)
        child.set('parent', self)
        if self.get('hierachy') is None:
            child.set('hierachy', 1)
        else:
            child.set('hierachy', self.get('hierachy')+1)
        return

    def removeChild(self, child):
        self.children.remove(child)
        return

    def getChild(self):
        return self.children

    def getCopy(self):
        copy = XmlElement(self.get('category'))
        for attribute in self.attributes:
            copy.set(attribute, self.get(attribute))
        for meta in self.meta:
            copy.set(meta, self.get(meta))
        for child in self.getChild():
            copyChild = child.getCopy()
            copy.addChild(copyChild)
        return copy
    
